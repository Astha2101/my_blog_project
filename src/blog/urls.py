from django.urls import path
from .views import post_detail, frontpage

app_name='blog'

urlpatterns=[
    path('',frontpage,name='frontpage'),
    path('<slug:slug>/',post_detail,name='post_detail'),
]