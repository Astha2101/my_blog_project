#### Blogging Website

### Main Feature:
- View Blogs 
- Add Comments

<br>

### Development Dependencies :
Set up a basic server using Python (v3.6) and Django (v2.2.5), and
install postgresql database and psycopg2 library.

<br>
### Endpoints :

- `GET` **/<slug>** - To get a post of associated with specified slug
- `PUT` **/<slug>/update** - To update contents of the specified slug. Takes `{"title": "intro", "body": "date_added"}` in json body.
- `DELETE` **/<slug>/delete>** - To delete the post, takes slug in url.
- `POST` **/create** - To create post. Takes `{"title": "intro", "body": "date_added"}` in json body. 

<br>

# Getting Started

First clone the repository from Github and switch to the new directory:

    $ git clone https://Astha2101@bitbucket.org/Astha2101/myblogproject.git
    $ cd src
    
Activate the virtualenv for your project.
    
Install project dependencies:

    $ pip install -r requirements/local.txt
    
    
Then simply apply the migrations:

    $ python manage.py migrate
    

You can now run the development server:

    $ python manage.py runserver